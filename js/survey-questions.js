/* ===== ADD ALL THE QUESTIONS HERE ===== */

// Page 1
addQuestion("1", "1", "radio", 
   "Do you currently have a food-producing garden?", 
   [
      "Yes", 
      "I'm interested but I haven't yet or can't", 
      "No, I'm not interested"
   ]);

addQuestion("1", "2", "radio", 
   "What is your age range?", 
   [
      "Under 18", 
      "18-24", 
      "25-34",
      "35-44",
      "45-54",
      "55-64",
      "65+",
      "Prefer not to say"
   ]);

addQuestion("1", "3", "radio",
   "What is your household situation?",
   [
      "Live alone",
      "other",
      "whatever. remove this...?"
   ]);


// Page 2
addQuestion("2", "1", "checkbox",
   "What motivates you to grow food?",
   [
      "I like it",
      "It's nice"
   ]);

addQuestion("2", "2", "radio",
   "Are you interested in increasing the productivity of your garden?",
   [
      "Sure!",
      "Maybe, idk",
      "Nope"
   ]);
   

// Page 3
addQuestion("3", "1", "checkbox",
   "How can we help?",
   [
      "You can't",
      "All hope is lost"
   ]);


// Page 4
addQuestion("4", "1", "checkbox",
   "What do u think abou the crop exchange?",
   [
      "Sounds aight",
      "Not so hot",
      "Count me in",
      "It fucks",
      "I fuck",
      "Eleven"
   ]);


// Page 5
addQuestion("5", "1", "checkbox",
   "What the fuck is wrong with you?",
   [
      "It's my parent's fault",
      "It's my fault"
   ]);


// Page 6
addQuestion("6", "1", "checkbox",
   "What challenges do you face?",
   [
      "Social anxiety",
      "Missing some fingers here and there",
      "Lactose intolerant"
   ]);