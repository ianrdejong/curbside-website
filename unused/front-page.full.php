
<?php get_header(); // in the HTML body tag now ?>

<!-- Stylesheet containing some icons including a hamburger menu -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- Import the front page js -->
<script src="wp-content/themes/Curbsite/js/fp-master.js" type="module"></script>

<script>
   function myFunction(){
   document.getElementById("fp-dropdown-content-ID").classList.toggle("show");
   console.log(document.getElementById("fp-dropdown-content-ID").classList)
}
</script>
<!-- -->
<div id="fp-bg">
   <div id="fp-bg-img-1" class="fp-bg-img"></div>
   <div id="fp-bg-img-2" class="fp-bg-img"></div>
   <div id="fp-bg-img-3" class="fp-bg-img"></div>
   <div id="fp-bg-img-4" class="fp-bg-img"></div>
</div>

<div id="fp-navbar">
     
   <div class="fp-dropdown">
      <button onclick="myFunction()">
      <i class="fa fa-bars"></i> 
         <div class="fp-navbar-hamburger"></div>
         <div class="fp-navbar-hamburger"></div>
         <div class="fp-navbar-hamburger"></div>
      </button>
   </div>
   <div id="fp-dropdown-content-ID" class="fp-dropdown-content">
      <a href="#">Link 1</a>
      <a href="#">Link 2</a>
      <a href="#">Link 3</a>
   </div>
</div>


<div id="fp-main">
   <div class="fp-content" id="fp-content-1">
      <h1>Our Roots</h1>
      <div class='divider'><span></span></div>
      <p>We are a growing community of gardeners working to transform urban greenspaces into lush food-producing microfarms. We believe in growing our own food, right here in the city, and sharing it with our communities.</p>
      
   </div>

   <div class="fp-content" id="fp-content-2">
      <h1>Our Process</h1>
      <div class='divider'><span></span></div>
      <p>We are a cooperative and that means everyone has a say. Members have the opportunity to share their gardening knowledge and exchange fresh produce with other members. Some of us even get together and construct garden beds out of used wooden pallets for the community.</p>
   </div>

   <div class="fp-content" id="fp-content-3">
      <h1>Our Process</h1>
      <div class='divider'><span></span></div>
      <p>A key part of our project is a website that will help bring members together to exchange tips, tricks and food if they so desire. It will also allow members to make decisions about how the cooperative should be run and what kinds of things we should do.</p>
   </div>

   <div class="fp-content" id="fp-content-4">
      <h1>We Need Your Help!</h1>
      <div class='divider'><span></span></div>
      <p>Send us your email and we'll be in touch!</p>
      <?php //echo do_shortcode("[RM_Form id='4']"); ?>
   </div>
</div>

<div><h1>TEST</h1></div>



<div id = "tall"></div>
   

