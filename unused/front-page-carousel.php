<?php get_header(); // in the HTML body tag now ?>

<!-- Import the front page js -->
<script src="/wp-content/themes/Curbsite/js/fp-new.js" type="module"></script>

<div id="fp-master-container">
   <?php insert_nav_menu(); ?>
   <script>
      // Change the id of the nav menu so that it gets styled with a custom front-page version.
      document.getElementById('nav-menu-bar').id = 'fp-nav-menu-bar';
   </script>
   <div class="fp-section" id="fp-image-container">
      <div class="fp-image-transition-wrapper">
         <img src="" class="fp-image-single">
      </div>

      <div class="fp-image-transition-wrapper">
         <img src="" class="fp-image-single">
      </div>

      <div class="fp-image-transition-wrapper">
         <img src="" class="fp-image-single">
      </div>

      <div class="fp-image-transition-wrapper">
         <img src="" class="fp-image-single">
      </div>
   </div>

   <div class="fp-section" id="fp-text-container">
      <div class="fp-content-single" id="fp-content-1">
         <h1>Our Roots</h1>
         <div class='divider'><span></span></div>
         <p>We are a growing community of gardeners working to transform urban greenspaces into lush food-producing microfarms. We believe in growing our own food, right here in the city, and sharing it with our communities.</p>
      </div>

      <div class="fp-content-single" id="fp-content-2">
         <h1>Our Process</h1>
         <div class='divider'><span></span></div>
         <p>We are a cooperative and that means everyone has a say. Members have the opportunity to share their gardening knowledge and exchange fresh produce with other members. Some of us even get together and construct garden beds out of used wooden pallets for the community.</p>
      </div>

      <div class="fp-content-single" id="fp-content-3">
         <h1>Our Process</h1>
         <div class='divider'><span></span></div>
         <p>A key part of our project is a website that will help bring members together to exchange tips, tricks and food if they so desire. It will also allow members to make decisions about how the cooperative should be run and what kinds of things we should do.</p>
      </div>

      <div class="fp-content-single" id="fp-content-4">
         <h1>We Need Your Help!</h1>
         <div class='divider'><span></span></div>
         <p>Send us your email and we'll be in touch!</p>
         <?php echo do_shortcode("[RM_Form id='4']"); ?>
      </div>
   </div>
</div>

<div id = "fp-document-height"></div>